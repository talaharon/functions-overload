/* eslint-disable */
//---------------------------------------------
//     Function overloads challenge
//---------------------------------------------
/**
 * Implement a function called *random* which returns a random number
 * we can optionaly pass a *max* range to pick from
 * we can optionaly pass a *min* number to start the range
 * we can optionally pass a *float* boolean to indicate if we want a float (true) or an integer (false)
 *   the *default is false* for the float boolean
 * if we call random with *no parameters*, it should *return either 0 or 1*
 * -----------------------
 * implement the function and call it in all variations
 * log the results for the different use cases
 */

// const num1 = random(); // 0 ~ 1 | integer
// const num2 = random(true); // 0 ~ 1 | float
// const num3 = random(false, 6); // 0 ~ 6 | integer,
// const num4 = random(false, 2, 6); // 2 ~ 6 | integer
// const num5 = random(true, 6); // 0 ~ 6 | float
// const num6 = random(true, 2, 6); // 2 ~ 6 | float

// console.log({ num1, num2, num3, num4, num5, num6 });

//---------------------------------------------------------------

function random(): number;
function random(floatFlag: boolean): number;
function random(floatFlag: boolean, max: number): number;
function random(floatFlag: boolean, min: number, max: number): number;

function random(floatFlag: boolean = false, minOrMax?: number, max?: number): number {
    const parseFunc = floatFlag ? Number : Math.round;
    let minNum: number = 0;
    let maxNum: number = 1;
    if (minOrMax !== undefined && max === undefined) {
        maxNum = minOrMax;
    } else if (minOrMax !== undefined && max !== undefined) {
        minNum = minOrMax;
        maxNum = max;
    }
    return parseFunc(Math.random() * (maxNum - minNum) + minNum);
}


const num1 = random(); // 0 ~ 1 | integer
const num2 = random(true); // 0 ~ 1 | float
const num3 = random(false, 6); // 0 ~ 6 | integer,
const num4 = random(false, 2, 6); // 2 ~ 6 | integer
const num5 = random(true, 6); // 0 ~ 6 | float
const num6 = random(true, 2, 6); // 2 ~ 6 | float
const num7 = random()

console.log({ num1, num2, num3, num4, num5, num6 });